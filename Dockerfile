###############################################################################

FROM archlinux:base-devel

###############################################################################

COPY "files/aurpkg.sh" "/usr/local/bin/aurpkg"
COPY "files/runmakepkg.sh" "/usr/local/bin/runmakepkg"
COPY "files/makepkg_pacman.sudoers" "/etc/sudoers.d/makepkg_pacman"

###############################################################################

RUN useradd                                                            \
      --create-home                                                    \
      --comment "Archlinux Makepkg User"                               \
      --groups "users"                                                 \
      "makepkg"                                                     && \
    chmod 755 "/usr/local/bin/aurpkg"                               && \
    chmod 755 "/usr/local/bin/runmakepkg"                           && \
    chmod 600 "/etc/sudoers.d/makepkg_pacman"                       && \
    echo "[multilib]" >> "/etc/pacman.conf"                         && \
    echo "Include = /etc/pacman.d/mirrorlist" >> "/etc/pacman.conf" && \
    pacman -Syu --noconfirm

###############################################################################

USER "makepkg"

###############################################################################

RUN mkdir "/home/makepkg/build"

###############################################################################

WORKDIR "/home/makepkg/build"

###############################################################################

ENTRYPOINT [ "/usr/local/bin/runmakepkg" ]

###############################################################################
